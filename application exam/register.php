<?php
require_once './connection.php';
$errors = array();
    if(isset($_POST['submit']))
    {
        $username = $_POST["username"];
        $email = $_POST["email"];
        $password = $_POST["password"];
        $confirmpassword = $_POST["confirmpassword"];
        $conn = mysqli_connect('localhost','root','','rethink');
        if ($password !== $confirmpassword) {
            die('<script>alert("Password not match!")</script>');
        }
        if (count($errors) == 0) {
        $query = mysqli_query($conn,"INSERT INTO `rethink`(`username`, `email`, `password`, `confirmpassword`) VALUES ('$username','$email','$password','$confirmpassword')");
        echo ('<script>alert("Successful!")</script>');
        header('location: /rethink/home.php');
        }
}
?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>rethink</title>
<link href="register.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="POST" onsubmit="return handleSubmit()">
<div class="banner">
    <div class="title">
    <h1 class="miniblog">MiniBlog</h1>
    <ul>
        <p class="loginT"> Login </p>
    </ul>
    </div>

    <div class="container">
    <div class="form">
        <div class="loginF">
        <p>Login</p>
        </div>
        <div class="login-form">
        <form action="contact.html" autocomplete="off">
            <div class="input-container">
            <input type="username" name="username" class="input" placeholder="Username" />
            <label for="username"></label>
            <span></span>
            </div>
            <div class="input-container">
            <input type="email" name="email" class="input" placeholder="Enter Email" />
            <label for="email"></label>
            <span></span>
            </div>
            <div class="input-container">
            <input type="password" name="password" class="input" placeholder="Enter Password" />
            <label for="password"></label>
            <span></span>
            </div>
            <div class="input-container">
            <input type="password" name="confirmpassword" class="input" placeholder="Confirm Password" />
            <label for="confirmpassword"></label>
            <span></span>
            </div>
            <input type="submit" value="REGISTER" name="submit" class="btn">
            <p class="clogout"> Return to the <a href="/rethink/login.php">LOGIN PAGE</a> </p>
        </form>
        </div>
    </div>
    </div>
</body>

</html>
