<?php
require_once 'dbconnect.php';
require_once 'function.php';
$result = display_data();
?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>rethink</title>
<link href="home.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="POST" onsubmit="return handleSubmit()">
<div class="banner">
    <div class="title">
    <h1 class="miniblog">MiniBlog</h1>
    <ul>
        <p class="loginT"> Login </p>
    </ul>
    </div>

    <div class="container">
    <div class="form">
        <div class="loginF">
        <p>POST TITLE</p>
        <table class="table table-bordered text-center">
        <tr class="bg-dark text-white">
        <td>id</td>
            <td>Title</td>
            <td>Content</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
            <?php
            while ($row = mysqli_fetch_assoc($result))
            {
                ?>
            <tr class="bg-dark text-white">
            <td><?php echo $row['id']; ?></td>
                <td><?php echo $row['title']; ?></td>
                <td><?php echo $row['content']; ?></td>
                <td><button class="btn btn-primary"><a href="update.php?id=<?php echo $row['id'] ?>" class="text-light">Edit</a></button></td>
                <td><button class="btn btn-danger"><a href="delete.php?id=<?php echo $row['id'] ?>"  class="text-light">delete</a></button></td></tr>
            <?php
            }
                ?>
        </table>
        </div>
    </div></div>
    <div class="container">
    <div class="form">
        <div class="login-form">
        <form action="contact.html" autocomplete="off">
            <a href="/rethink/createpost.php">
            <button type="button"><span></span>Create a New Post</button>
            </a>
        </form>
        </div>
    </div>
    </div>
</body>

</html>
