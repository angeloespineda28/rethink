<?php
require_once './connection.php';
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = sanitize_data($_POST["email"]);
    $password = sanitize_data($_POST["password"]);
    $error = [];

    if (count($error) == 0) {

        //Preparing Stage
        $sql = "SELECT email, password FROM rethink WHERE email = ? AND password = ?";
        if ($stmt = mysqli_prepare($conn, $sql)){
            mysqli_stmt_bind_param($stmt, 'ss', $email, $password);

            //Execution Stage
            if (mysqli_stmt_execute($stmt)){
                mysqli_stmt_store_result($stmt);

                //Checks if the results returns a row
                if (mysqli_stmt_num_rows($stmt) == 1) {
                    //Binds the Result to a custom variables
                    mysqli_stmt_bind_result($stmt, $email, $password);

                    //Fetches the result
                    if (mysqli_stmt_fetch($stmt)) {
                        
                        header('location: /rethink/home.php');
                    }
                }

                else {
                    die('<script>alert("Username or Password is incorrect!")</script>');
                }
            }
        }
    }

    if (count($error) > 0) {
        echo json_encode($error);
    }
}

function sanitize_data($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlentities($data);
    return $data;
}
?>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>rethink</title>
<link href="login.css" rel="stylesheet" type="text/css" />
</head>

<body>
<form method="POST" onsubmit="return handleSubmit()">
<div class="banner">
    <div class="title">
    <h1 class="miniblog">MiniBlog</h1>
    <ul>
        <p class="loginT"> Login </p>
    </ul>
    </div>

    <div class="container">
    <div class="form">
        <div class="loginF">
        <p>Login</p>
        </div>
        <div class="login-form">
        <form action="contact.html" autocomplete="off">
            <div class="input-container">
            <input type="email" name="email" class="input" placeholder="Enter Email" />
            <label for="email"></label>
            <span></span>
            </div>
            <div class="input-container">
            <input type="password" name="password" class="input" placeholder="Enter Password" />
            <label for="password"></label>
            <span></span>
            </div>
            <input type="submit" value="Login" name="login" class="btn">
            <a href="/rethink/register.php">
            <button type="button"><span></span>Register</button>
            </a>
            <p class="clogout"> currently logged out </p>
        </form>
        </div>
    </div>
    </div>
</body>

</html>
